/*
    Author: Bryan Gillespie
 */

package com.gillespie.bryan.smswizard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/*
    This is the Adapter needed for RecyclerView
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder> {

    private Context context;
    private List<ScheduledMessage> messages;
    private MessageDBHandler dbHandler;

    public RecyclerViewAdapter(Context context, List<ScheduledMessage> messages)
    {
        this.context = context;
        this.messages = messages;
        dbHandler = new MessageDBHandler(context);
    }

    //Used to update the RecyclerView if a new entry is added
    public void setMessages(List<ScheduledMessage> messages)
    {
        this.messages = messages;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Somewhat boilerplate - inflates the custom layout for each item
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_row, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        ScheduledMessage message = messages.get(position);

        //Only show a preview if too long...
        String messageBody = message.getMessage();
        if (messageBody.length() > 50)
            messageBody = messageBody.substring(0, 47) + "...";

        String condition = "";

        if(message.getType().equals("Time")) {
            holder.icon.setImageResource(R.drawable.time_icon);
            Date date = new Date(message.getTime());
            condition = date.toString();
        } else {
            holder.icon.setImageResource(R.drawable.location_icon);
            DecimalFormat df1 = new DecimalFormat(".#");
            condition = message.getPlaceName() + " (" + df1.format(message.getRadius()) + " mi)";
        }

        holder.phoneNumber.setText(message.getRecipient());
        holder.condition.setText(condition);
        holder.message.setText(messageBody);
        holder.container.setBackgroundColor(ContextCompat.getColor(context, R.color.cardview_light_background));

        if(message.isSent())
            holder.container.setAlpha(0.2f);
        else
            holder.container.setAlpha(1f);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void removeItem(int position) {
        dbHandler.delete(messages.get(position));
        messages.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    //This is the ViewHolder for each item in the RecyclerView
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView phoneNumber;
        TextView condition;
        TextView message;
        ImageView icon;

        public CustomViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.Container);
            phoneNumber = itemView.findViewById(R.id.PhoneNumber);
            condition = itemView.findViewById(R.id.Condition);
            message = itemView.findViewById(R.id.Message);
            icon = itemView.findViewById(R.id.Icon);

            //Create a listener for when the user taps the item
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //If the user clicked this item, goto the View Entry activity
                    Intent intent = new Intent(view.getContext(), ViewMessageActivity.class);
                    intent.putExtra("ENTRY_ID", "" + messages.get(getAdapterPosition()).getId()); //Pass the ID so the activity knows which one to display
                    view.getContext().startActivity(intent);
                }
            });
        }
    }

}

/*
    Author: Bryan Gillespie
 */

package com.gillespie.bryan.smswizard;

/*
    This is a simple data structure for holding scheduled messages.

    It is used by both the RecyclerView Adapter and the SQLite helper
 */

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "message_table")
public class ScheduledMessage
{
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "recipient")
    private String recipient;
    @ColumnInfo(name = "message")
    private String message;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "place_name")
    private String placeName;
    @ColumnInfo(name = "latitude")
    private double latitude;
    @ColumnInfo(name = "longitude")
    private double longitude;
    @ColumnInfo(name = "radius")
    private double radius;
    @ColumnInfo(name = "time")
    private long time;
    @ColumnInfo(name = "sent")
    private boolean sent;
    @ColumnInfo(name = "sent_time")
    private long sentTime;

    public ScheduledMessage() {}

    public ScheduledMessage(String recipient, String message, String placeName, double latitude, double longitude, double radius)
    {
        id = 0;
        this.recipient = recipient;
        this.message = message;
        type = "Location";
        this.placeName = placeName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        time = 0;
        sent = false;
        sentTime = 0;
    }

    public ScheduledMessage(String recipient, String message, long time)
    {
        id = 0;
        this.recipient = recipient;
        this.message = message;
        type = "Time";
        this.time = time;
        this.placeName = "";
        this.latitude = 0;
        this.longitude = 0;
        this.radius = 0;
        sent = false;
        sentTime = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public long getSentTime() {
        return sentTime;
    }

    public void setSentTime(long sentTime) {
        this.sentTime = sentTime;
        if(sentTime > 0)
            sent = true;
    }
}

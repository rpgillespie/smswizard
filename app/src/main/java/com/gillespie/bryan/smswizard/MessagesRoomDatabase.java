package com.gillespie.bryan.smswizard;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.Update;
import android.content.Context;

import java.util.List;

@Database(entities = {ScheduledMessage.class}, version = 1)
public abstract class MessagesRoomDatabase extends RoomDatabase {

    public abstract MessageDao messageDao();
    private static MessagesRoomDatabase INSTANCE;

    @Dao
    public interface MessageDao
    {
        @Insert
        long insert(ScheduledMessage message);

        @Query("SELECT * from message_table ORDER BY sent ASC, sent_time ASC , time ASC, id DESC")
        List<ScheduledMessage> getAllMessages();

        @Query("SELECT * from message_table WHERE id = :theID")
        ScheduledMessage getMessageByID(int theID);

        @Update
        void update(ScheduledMessage message);

        @Delete
        void delete(ScheduledMessage message);
    }

    static MessagesRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MessagesRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MessagesRoomDatabase.class, "messages_database").allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
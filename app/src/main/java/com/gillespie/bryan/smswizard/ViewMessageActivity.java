/*
    Author: Bryan Gillespie
 */

package com.gillespie.bryan.smswizard;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Date;

/*
    View a diary entry
 */
public class ViewMessageActivity extends FragmentActivity implements OnMapReadyCallback {

    private MessageDBHandler dbHandler;
    private String id;
    private ScheduledMessage msg;

    private TextView recipientText;
    private TextView messageBody;
    private TextView sentText;
    private Button editButton;
    private TextView conditionText;
    private TextView dateText;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_message);

        // Show the Up button in the action bar.
        ActionBar actionBar = getActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            id = extras.getString("ENTRY_ID"); //Retrieve entry ID
        } else {
            id = (String) savedInstanceState.getSerializable("ENTRY_ID");
        }

        recipientText = findViewById(R.id.RecipientText);
        messageBody = findViewById(R.id.MessageBody);
        sentText = findViewById(R.id.SentText);
        conditionText = findViewById(R.id.Condition);
        dateText = findViewById(R.id.DateText);
        editButton = findViewById(R.id.EditButton);

        dbHandler = new MessageDBHandler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }

    private void updateView() {
        //Get the entry based on which one they tapped
        msg = dbHandler.getMessageByID(Integer.parseInt(id));

        recipientText.setText(msg.getRecipient());
        messageBody.setText(msg.getMessage());

        if(msg.isSent()) {
            editButton.setVisibility(View.GONE);
            editButton.setClickable(false);
            Date date = new Date(msg.getSentTime());
            sentText.setText("Sent on " + date.toString());
        } else {
            sentText.setVisibility(View.GONE);
        }

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if(msg.getType().equals("Location")) {
            dateText.setVisibility(View.GONE);
            if(msg.isSent())
                conditionText.setText("Message was sent around here: ");
            else
                conditionText.setText("Message will be sent upon arriving here: ");
        } else {
            Date date = new Date(msg.getTime());
            dateText.setText(date.toString());
            mapFragment.getView().setVisibility(View.GONE);
            if(msg.isSent())
                conditionText.setText("Message was sent at: ");
            else
                conditionText.setText("Message is scheduled to be sent at: ");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in our note location and move the camera
        LatLng loc = new LatLng(msg.getLatitude(), msg.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(loc));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 13));
    }

    public void onEdit(View v) {
        Intent intent = new Intent(this, NewMessageActivity.class);
        intent.putExtra("ENTRY_ID", "" + msg.getId()); //Pass the ID so the activity knows which one to display
        startActivity(intent);
    }
}

/*
    Author: Bryan Gillespie
 */

package com.gillespie.bryan.smswizard;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Calendar;

/*
    Create a new scheduled message
 */
public class NewMessageActivity extends AppCompatActivity implements OnMapReadyCallback {

    private final static int PLACE_PICKER_REQUEST = 999;

    private EditText recipientInput;
    private EditText messageInput;
    private TextView error;
    private RadioGroup radio;
    private TextView dateText;
    private SupportMapFragment mapFragment;
    GoogleMap googleMap;
    private TextView radiusInput;
    private TextView radiusLabel1;
    private TextView radiusLabel2;

    private double latitude;
    private double longitude;
    private String placeName;
    private Calendar date;

    private MessageDBHandler dbHandler;
    private String id;
    private ScheduledMessage updatingMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);

        // Show the Up button in the action bar.
        ActionBar actionBar = getActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        id = "";
        if (savedInstanceState == null) {
            if(getIntent().hasExtra("ENTRY_ID"))
                id = getIntent().getStringExtra("ENTRY_ID"); //Retrieve entry ID
        }
        //else
            //id = (String) savedInstanceState.getSerializable("ENTRY_ID");

        dbHandler = new MessageDBHandler(this);

        recipientInput = findViewById(R.id.RecipientInput);
        messageInput = findViewById(R.id.MessageInput);
        error = findViewById(R.id.ErrorText);
        radio = findViewById(R.id.RadioGroup);
        dateText = findViewById(R.id.DateCondition);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.MapCondition);
        radiusInput = findViewById(R.id.RadiusInput);
        radiusLabel1 = findViewById(R.id.WithinLabel);
        radiusLabel2 = findViewById(R.id.MilesLabel);

        initializeFields();
        updateConditionPanel();

        mapFragment.getMapAsync(this);
        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                updateConditionPanel();
            }
        });
    }

    private void initializeFields() {
        date = Calendar.getInstance();
        if(id.isEmpty()) {
            date.set(Calendar.HOUR, (date.get(Calendar.HOUR) + 1) % 23);
            date.set(Calendar.SECOND, 0);
            latitude = -33.86;
            longitude = 151.2;
            radiusInput.setText("20");
        } else {
            updatingMsg = dbHandler.getMessageByID(Integer.parseInt(id));
            recipientInput.setText(updatingMsg.getRecipient());
            messageInput.setText(updatingMsg.getMessage());
            date.setTimeInMillis(updatingMsg.getTime());
            latitude = updatingMsg.getLatitude();
            longitude = updatingMsg.getLongitude();
            radiusInput.setText("" + updatingMsg.getRadius());

            if(updatingMsg.getType().equals("Location"))
                radio.check(R.id.LocationRadio);

            ActionBar actionBar = getActionBar();
            if (actionBar != null)
                actionBar.setTitle("Update Message");

            ((Button)findViewById(R.id.SaveButton)).setText("Update");
        }
        dateText.setText(date.getTime().toString());
    }

    private void updateConditionPanel() {
        if(isUsingTimeCondition()) {
            setMapVisibility(View.GONE);
            dateText.setVisibility(View.VISIBLE);
        } else {
            setMapVisibility(View.VISIBLE);
            dateText.setVisibility(View.GONE);
        }
    }

    private void setMapVisibility(int visibility) {
        radiusInput.setVisibility(visibility);
        radiusLabel1.setVisibility(visibility);
        radiusLabel2.setVisibility(visibility);
        mapFragment.getView().setVisibility(visibility);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //checkPermissionOnActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode){
                case PLACE_PICKER_REQUEST:
                    Place place = PlacePicker.getPlace(this, data);
                    placeName = String.format("%s", place.getName());
                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;

                    LatLng loc = new LatLng(latitude, longitude);
                    googleMap.addMarker(new MarkerOptions().position(loc));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 13));
            }
        }
    }

    public void showDateTimePicker() {
        final Context context = this;
        DatePickerDialog dpDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        dateText.setText(date.getTime().toString());
                    }
                }, date.get(Calendar.HOUR_OF_DAY), date.get(Calendar.MINUTE), false).show();
            }
        }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dpDialog.show();
    }

    public void showPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        // Add a marker in our note location and move the camera
        LatLng loc = new LatLng(latitude, longitude);
        googleMap.addMarker(new MarkerOptions().position(loc));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 13));
    }

    public void onEdit(View v) {
        if(radio.getCheckedRadioButtonId() == R.id.TimeRadio)
            showDateTimePicker();
        else
            showPlacePicker();
    }

    public void onSave(View v) {
        String rec = recipientInput.getText().toString();
        String ent = messageInput.getText().toString();
        error.setText("");

        //Don't allow empty subjects or bodies
        if (rec.isEmpty() || ent.isEmpty()) {
            error.setText("Missing recipient phone number or message body");
            return;
        }

        //If not valid phone number error out
        if (rec.length() < 10) {
            error.setText("Not a valid phone number");
            return;
        }

        //If body is too long, error out
        if (ent.length() > 420) {
            error.setText("Entry cannot be more than 420 chars");
            return;
        }

        if (date.getTimeInMillis() - System.currentTimeMillis() < 0) {
            error.setText("Can't schedule messages in the past; only the future");
            return;
        }

        if(updatingMsg != null) {
            onUpdate(rec, ent);
            return;
        }

        //Add entry to database
        ScheduledMessage message;
        if(isUsingTimeCondition())
            message = new ScheduledMessage(rec, ent, date.getTimeInMillis());
        else
            message = new ScheduledMessage(rec, ent, placeName, latitude, longitude, Double.parseDouble(radiusInput.getText().toString()));

        int id = (int) dbHandler.insert(message);

        //Schedule the service to carry it out
        JobScheduler mJobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        JobInfo.Builder builder = new JobInfo.Builder(id, new ComponentName(getPackageName(), JobSchedulerService.class.getName()));
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setMinimumLatency((message.getType() == "Time") ? (message.getTime() - System.currentTimeMillis()) : 3000);
        mJobScheduler.schedule(builder.build());

        //Display a toast notification before returning
        Toast.makeText(getApplicationContext(), "New Message Scheduled", Toast.LENGTH_SHORT).show();

        finish();
    }

    void onUpdate(String rec, String body)
    {
        boolean timeType = isUsingTimeCondition();

        updatingMsg.setRecipient(rec);
        updatingMsg.setMessage(body);
        updatingMsg.setType(timeType ? "Time" : "Location");
        updatingMsg.setPlaceName(timeType ? "" : placeName);
        updatingMsg.setLatitude(timeType ? 0 : latitude);
        updatingMsg.setLongitude(timeType ? 0 : longitude);
        updatingMsg.setTime(timeType ? date.getTimeInMillis() : 0);
        updatingMsg.setRadius(timeType ? 0 : Double.parseDouble(radiusInput.getText().toString()));

        dbHandler.update(updatingMsg);

        JobScheduler mJobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        //Cancel current job
        mJobScheduler.cancel(updatingMsg.getId());

        //Schedule new one
        JobInfo.Builder builder = new JobInfo.Builder(updatingMsg.getId(), new ComponentName(getPackageName(), JobSchedulerService.class.getName()));
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setMinimumLatency((updatingMsg.getType() == "Time") ? (updatingMsg.getTime() - System.currentTimeMillis()) : 3000);
        mJobScheduler.schedule(builder.build());

        //Display a toast notification before returning
        Toast.makeText(getApplicationContext(), "Message Updated", Toast.LENGTH_SHORT).show();

        finish();
    }

    boolean isUsingTimeCondition() {
        return radio.getCheckedRadioButtonId() == R.id.TimeRadio;
    }
}

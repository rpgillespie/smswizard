package com.gillespie.bryan.smswizard;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

public class JobSchedulerService extends JobService {

    private final double MILES_PER_METER = 0.000621371;

    String CHANNEL_ID = "SMSWizard";

    private LocationManager locationManager;
    private LocationListener listener;
    JobParameters jp;
    MessageDBHandler dbHandler;
    ScheduledMessage m;

    @Override
    public boolean onStartJob(JobParameters params) {
        jp = params;
        int id = params.getJobId();

        dbHandler = new MessageDBHandler(this);
        m = dbHandler.getMessageByID(id);

        if (m.isSent())
            return false;

        if (m.getType().equals("Time") && m.getTime() <= System.currentTimeMillis()) {
            sendSMS();
            return false;
        }
        else if (m.getType().equals("Location")) {
            setupLocation();
            return true;
        }

        return false;
    }

    private void setupLocation() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Location loc1 = new Location("");
                loc1.setLatitude(location.getLatitude());
                loc1.setLongitude(location.getLongitude());

                Location loc2 = new Location("");
                loc2.setLatitude(m.getLatitude());
                loc2.setLongitude(m.getLongitude());

                double distance_miles = loc1.distanceTo(loc2) * MILES_PER_METER;

                if (distance_miles <= m.getRadius()) {
                    sendSMS();
                    locationManager.removeUpdates(listener);
                    jobFinished(jp, false);
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {}

            @Override
            public void onProviderEnabled(String s) {}

            @Override
            public void onProviderDisabled(String s) {
                //If we are here, the location provider is disabled! Take user to enable it.
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };

        //Request both network and gps time in case one is not available
        locationManager.requestLocationUpdates("gps", 15000, 0, listener);
        locationManager.requestLocationUpdates("network", 15000, 0, listener);
    }

    private void sendSMS() {
        // Send SMS
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(m.getRecipient(), null, m.getMessage(), null, null);

        // Update database
        m.setSentTime(System.currentTimeMillis());
        m.setSent(true);
        dbHandler.update(m);

        sendNotification();
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    public void sendNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);

        //Create the intent that’ll fire when the user taps the notification//

        /*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.androidauthority.com/"));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        mBuilder.setContentIntent(pendingIntent);*/

        mBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
        mBuilder.setContentTitle("SMS Wizard");
        mBuilder.setContentText("Pending Text Sent!");
        mBuilder.setPriority(NotificationCompat.PRIORITY_MAX);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(001, mBuilder.build());
    }

}
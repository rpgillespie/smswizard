/*
    Author: Bryan Gillespie
 */

package com.gillespie.bryan.smswizard;

import android.app.Application;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/*
    SQLite DB helper - used to interface with DB
 */
public class MessageDBHandler
{
    private static MessagesRoomDatabase.MessageDao dao;

    public MessageDBHandler(Context context)
    {
        dao = MessagesRoomDatabase.getDatabase(context).messageDao();
    }

    public List<ScheduledMessage> getAllEntries() {
        List<ScheduledMessage> items = dao.getAllMessages();

        //For testing
//        if(items.size() == 0)
//            addDummyContent();

        return items;
    }

    public long insert (ScheduledMessage msg) {
        return dao.insert(msg);
    }

    public ScheduledMessage getMessageByID(int id) {
        return dao.getMessageByID(id);
    }

    public void update(ScheduledMessage msg) {
        dao.update(msg);
    }

    public void delete(ScheduledMessage msg) {
        dao.delete(msg);
    }

    private List<ScheduledMessage> addDummyContent() {
        dao.insert(new ScheduledMessage("8012349832", "Did you get the job done?", System.currentTimeMillis() + 1000000));
        dao.insert(new ScheduledMessage("4562346781", "I made it safely!", "45 Crested Iris Ct", 52.46, 0.54, 20));
        ScheduledMessage sentMessage = new ScheduledMessage("1234567890", "Where are you?", "The White House", -33.86, 151.2, 20);
        sentMessage.setSentTime(System.currentTimeMillis() - 1000000);
        dao.insert(sentMessage);
        return dao.getAllMessages();
    }
}
